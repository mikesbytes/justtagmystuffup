      ____  ______  ___ ___  _____ __ __ 
     |    ||      ||   |   |/ ___/|  |  |
     |__  ||      || _   _ (   \_ |  |  |
     __|  ||_|  |_||  \_/  |\__  ||  |  |
    /  |  |  |  |  |   |   |/  \ ||  :  |
    \  `  |  |  |  |   |   |\    ||     |
     \____j  |__|  |___|___| \___| \__,_|

Just Tag My Stuff Up (JTMSU) is an image tagging and management system.
It is currently in unusable alpha stage but current features include

- Scanning specified folders on the local filesystem for media
- Quickly index large libraries
- Automatic thumbnail generation
- Tag system
- Search based on tags or filenames
- Shiny (remotely accessible) web interface!
- REST API for writing your own tools

#Motivation

Over the years, I have begun to accumulate a lot of images. Things saved
from websites, stuff I've made, and personal photos. It has always been
a pain to find that *one* image that I know I have somewhere, but it could
be in any one of dozens of folders depending on where it came from or where
I saved it. Having tried many existing solutions, I set out to make my own,
to do what nobody else can seem to get right.

#Goals

Here are the things I haven't done yet that I want to do

- Duplicate detection (both file and image hash based)
- Detect moved files
- Live scanning
- Web scraper for downloading and auto-tagging images