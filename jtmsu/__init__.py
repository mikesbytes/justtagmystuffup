import os

from flask import Flask
from flask_restful import Api

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'jtmsu.sqlite'),
        THUMBNAIL_DIR=os.path.join(app.instance_path, 'thumbnails')
        )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # DB import
    from . import db
    db.init_app(app)

    from .import dbschema
    dbschema.init_app(app)

    # BP imports
    from . import file
    app.register_blueprint(file.bp)

    # REST API imports
    api = Api(app)

    from . import filesapi
    api.add_resource(filesapi.FilesAPI, '/api/files/')

    from . import sourceapi
    sourceapi.register_resource(api)

    from . import sourcelistapi
    sourcelistapi.register_resource(api)

    from . import tagsapi
    tagsapi.register_resource(api)

    from . import tagapi
    tagapi.register_resource(api)

    return app
