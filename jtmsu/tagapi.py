from flask import Flask, send_from_directory, abort
from flask_restful import reqparse, Resource, Api

from jtmsu.dbschema import get_db, Tag

class TagAPI(Resource):
    def get(self, tag_id):
        db = get_db()
        db.connect()

        tag = Tag.select().where(Tag.id == tag_id)
        if not tag.exists():
            abort(404)

        tag = tag.get()

        db.close()
        return {
            'uri' : '/api/tag/' + str(tag_id),
            'id' : tag.id,
            'name' : tag.name
            }

    def delete(self, tag_id):
        pass

def register_resource(api):
    api.add_resource(TagAPI, '/api/tag/<int:tag_id>')
