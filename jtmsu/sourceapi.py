from flask import Flask, send_from_directory, abort
from flask_restful import reqparse, Resource, Api

from os import listdir, walk
from os.path import isfile, join
from jtmsu.dbschema import get_db, Source, File

import hashlib

parser = reqparse.RequestParser()
parser.add_argument('action')

class SourceAPI(Resource):
    def get(self, source_id):
        db = get_db()
        db.connect()

        source = Source.select().where(Source.id == source_id)
        if not source.exists():
            abort(404)

        source = source.get()

        db.close()

        return { 'uri' : 'api/source/' + str(source_id),
                 'id' : source.id,
                 'path' : source.path,
                 'type' : source.type
                 }

    def scan(self, source_id):
        db = get_db()
        db.connect()

        source = Source.get(Source.id == source_id)
        if not source:
            abort(404)

        # extensions to include
        extensions = ['.jpg', '.jpeg', '.gif', '.png']

        # list all files in the source dir
        file_list = [join(f, root) for root,dirs,files in walk(source.path) for f in files]

        new_files = 0
        BLOCKSIZE = 65536
        for root,dirs,files in walk(source.path):
            for f in files:
                if not isfile(join(root,f)):
                    continue
                full_path = join(root,f)
                existing = File.select().where(File.full_path == full_path)
                if not existing.exists():
                    # get sha1 of file
                    hasher = hashlib.sha1()
                    with open(full_path, 'rb') as afile:
                        buf = afile.read(BLOCKSIZE)
                        while len(buf) > 0:
                            hasher.update(buf)
                            buf = afile.read(BLOCKSIZE)

                    new_file = File(source = source, full_path=full_path, filename=f, name=f, sha1=hasher.hexdigest())
                    new_file.save()

                    new_files += 1

        db.close()
        return { 'new_file_count' : new_files }

    
    def post(self, source_id):
        args = parser.parse_args()
        action = args['action']
        if action == 'scan':
            return self.scan(source_id)

def register_resource(api):
    api.add_resource(SourceAPI, '/api/source/<int:source_id>')
