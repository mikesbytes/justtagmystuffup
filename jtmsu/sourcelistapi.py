from flask import Flask, send_from_directory, abort
from flask_restful import reqparse, Resource, Api

from os import listdir
from os.path import isfile, join
from jtmsu.dbschema import get_db, Source

parser = reqparse.RequestParser()
parser.add_argument('path')
parser.add_argument('type')

class SourceListAPI(Resource):
    def get(self):
        db = get_db()
        db.connect()
        sources = Source.select()

        slist = []
        for s in sources:
            slist.append({
                'uri' : '/api/source/' + str(s.id),
                'id' : s.id,
                'path' : s.path,
                'type' : s.type
                })

        db.close()
        return { 'sources' : slist }

    def post(self):
        db = get_db()
        db.connect()

        args = parser.parse_args()
        path = args['path']
        stype = args['type']

        existing = Source.select().where(path == path)

        if existing.exists():
            abort(404)

        new_source = Source(path = args['path'], type = args['type'])
        new_source.save()
        db.close()

def register_resource(api):
    api.add_resource(SourceListAPI, '/api/sourcelist')
