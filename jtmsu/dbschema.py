from peewee import *

from flask import current_app
from flask.cli import with_appcontext
from jtmsu.g import g
import click

def get_db():
    if not 'db' in g:
        g['db'] = SqliteDatabase('instance/jtsmu.db')
    return g['db']

class BaseModel(Model):
    class Meta:
        database = get_db()

class Source(BaseModel):
    path = TextField()
    type = TextField()

class File(BaseModel):
    source = ForeignKeyField(Source, backref='files')
    full_path = TextField()
    filename = TextField()
    name = TextField()
    sha1 = BlobField()

class Tag(BaseModel):
    name = TextField()

class TagLink(BaseModel):
    tag = ForeignKeyField(Tag, backref='links')
    file = ForeignKeyField(File, backref='links')

    class Meta:
        indexes = (
            (('tag', 'file'), True),
        )

def init_db():
    db = get_db()
    db.connect()

    db.create_tables([Source, File, Tag, TagLink])

    db.close()

@click.command('peewee-init')
@with_appcontext
def peewee_init_command():
    """Initialize Peewee Tables"""
    init_db()
    click.echo('DB Initialized')

def init_app(app):
    app.cli.add_command(peewee_init_command)
