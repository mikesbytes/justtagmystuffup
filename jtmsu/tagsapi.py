from flask import Flask, send_from_directory, abort, request
from flask_restful import reqparse, Resource, Api

from os import listdir
from os.path import isfile, join
from jtmsu.dbschema import get_db, Tag, TagLink

class TagsAPI(Resource):
    def get(self):
        db = get_db()
        db.connect()

        tagsq = Tag.select()
        tags = []
        for i in tagsq:
            tags.append({
                'uri' : '/api/tag/' + str(i.id),
                'id' : i.id,
                'name' : i.name
                })

        db.close()
        return { 'tags' : tags }

    def tag_items(self, pairs):
        db = get_db()
        db.connect()

        tagged = []
        for i in pairs:
            new_link = TagLink(tag = i['tag_id'], file = i['file_id'])
            new_link.save()

            tagged.append(i)

        db.close()
        return {'tagged_pairs' : tagged}

    def untag_items(self, pairs):
        db = get_db()
        db.connect()

        tagged = []
        for i in pairs:
            existing = db.execute('SELECT 1 FROM tag_link WHERE tag=? AND file=?',
                                  (i['tag_id'], i['file_id'])).fetchone()
            if existing:
                db.execute('DELETE FROM tag_link '
                        'WHERE tag=? '
                        'AND file=?',
                        (i['tag_id'], i['file_id']))
                tagged.append(i)

        # commit db if anything was tagged
        if len(tagged) > 0:
            db.commit()

        db.close()
        return {'untagged_pairs' : tagged}
    
    def create_tag(self, tag_name):
        db = get_db()
        db.connect()

        tagq = Tag.select().where(Tag.name == tag_name)
        if tagq.exists():
            abort(409)

        new_tag = Tag(name = tag_name)
        new_tag.save()

        db.close()
        return { 'tag' : {
            'uri' : '/api/tag/' + str(new_tag.id),
            'id' : new_tag.id,
            'name' : new_tag.name
        }}, 201
        
    
    def post(self):

        data = request.json
        action = data.get('action')
        if action == 'create':
            return self.create_tag(data['tag']['name'])
        elif action == 'tag_items':
            return self.tag_items(data['pairs'])
        elif action == 'untag_items':
            return self.untag_items(data['pairs'])
            

def register_resource(api):
    api.add_resource(TagsAPI, '/api/tags/')
