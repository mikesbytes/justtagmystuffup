from flask import Flask, send_from_directory, url_for, request
from flask_restful import Resource, Api

from os import listdir
from os.path import isfile, join

from jtmsu.dbschema import get_db, File, Tag, TagLink
from peewee import *

class FilesAPI(Resource):
    def search(self, term, start = 0, limit = 100):
        db = get_db()
        db.connect()

        files = []
        file_list = None # FILE * table query output
        
        if term.startswith('tag:'):
            # trim 'tag:' off term
            term = term[4:]

            #create list of tag names
            tag_names = [i.strip() for i in term.split(',')]
            tag_count = len(tag_names)

            # AND tag query
            file_list = (File
                         .select()
                         .join(TagLink)
                         .join(Tag)
                         .where(Tag.name.in_(tag_names))
                         .group_by(File.id)
                         .having(fn.COUNT(File.id) == tag_count)
                         .limit(limit).offset(start))

        elif term == "":
            file_list = File.select().limit(limit).offset(start)
        else:
            file_list = File.select().where(File.name.contains(term)).limit(limit).offset(start)

        for f in file_list:
            nf = {
                'id' : f.id,
                'path' : url_for('file.get_file', id=f.id),
                'full_path' : f.full_path,
                'filename' : f.filename,
                'name' : f.name,
                'sha1' : str(f.sha1),
                'tags' : []
            }
            tagq = Tag.select().join(TagLink, JOIN.LEFT_OUTER).where(TagLink.file == f)
            for t in tagq:
                nf['tags'].append({
                    'id' : t.id,
                    'name' : t.name
                })
            files.append(nf)

        db.close()
        return {'files' : files}
    
    def get(self):
        return self.search("")
        
    def post(self):
        data = request.json
        if 'search' in data:
            return self.search(data['search'], data['start'], data['limit'])
        abort(400)
