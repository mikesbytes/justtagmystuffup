import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, send_file, current_app, abort
)

from jtmsu.dbschema import get_db, File
import os
from PIL import Image

bp = Blueprint('file', __name__, url_prefix='/file')

@bp.route('/<int:id>')
def get_file(id):
    db = get_db()
    db.connect()
    f = File.get(File.id == id)
    
    db.close()
    return send_file(f.full_path)

@bp.route('/<int:id>/thumbnail/<int:size>')
def get_thumbnail(id, size):
    size2 = size, size
    max_per_folder = 500
    db = get_db()
    q = File.select().where(File.id == id)

    if not q.exists():
        abort(404)

    q = q.get()

    #construct thumbnail path
    path = os.path.join(
        current_app.config['THUMBNAIL_DIR'],
        str(size),
        str(id // max_per_folder)
        )
    fname = str(id) + ".jpg"
    full_path = os.path.join(path, fname)

    # ensure dir exists
    try:
        os.makedirs(path)
    except OSError:
        pass

    if not os.path.isfile(full_path):
        ext = os.path.splitext(q['full_path'])[1]
        try:
            if ext in ('.jpg', '.jpeg'):
                im = Image.open(q.full_path)
            if ext == '.png':
                im = Image.open(q.full_path).convert('RGB')
            if ext == '.gif':
                im = Image.open(q.full_path)
                bg = Image.new("RGB", im.size, (255,255,255))
                bg.paste(im)
                im = bg

            im.thumbnail(size2, Image.ANTIALIAS)
            im.save(full_path, 'JPEG', quality=80)

        except IOError:
            abort(404)

    db.close()
    return send_file(full_path)

